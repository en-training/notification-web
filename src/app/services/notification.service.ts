import { Injectable } from '@angular/core';
import {Observable, combineAll, of, take, tap, filter, map, BehaviorSubject} from 'rxjs';
import {
  SearchNotificationApiResponse,
  Notification,
  NotificationResponse,
  NotificationStatus,
  CreateNotificationCommand,
  CreateNotificationApiResponse,
  CountUnreadNotificationApiResponse, MarkNotificationApiResponse, MarkNotificationCommand
} from '../models/notification.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {

  notificationResponses: Array<NotificationResponse> = [];

  unreadNotificationsCountSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) { }

  getNotifications() : Array<Notification> {
    return [
      {
        title: 'New year event',
        message: 'The celebration will start @3:00 PM'
      }
    ]
  }

  createNotification(command: CreateNotificationCommand): Observable<CreateNotificationApiResponse> {
    return this.http.post<CreateNotificationApiResponse>('http://localhost:7072/notifications', command);
  }

  markStatusNotification(command: MarkNotificationCommand): Observable<MarkNotificationApiResponse> {
    return this.http.put<MarkNotificationApiResponse>('http://localhost:7072/notifications/mark-status', command);
  }

  fetchNotifications(): Observable<SearchNotificationApiResponse> {

    return this.http.get<SearchNotificationApiResponse>('http://localhost:7072/notifications')
    .pipe(
      take(1),
      tap((res) => {
        this.notificationResponses = res.data?.notificationResponses || [];
      })
    );
  }

  getNotificationById(notificationId: string): NotificationResponse {
    const result =  this.notificationResponses.find(notification => notification.notificationId == notificationId);

    return result || {} as NotificationResponse;
  }

  countUnreadNotifications() {
    this.http.get<CountUnreadNotificationApiResponse>("http://localhost:7072/notifications/count-un-read-notifications")
      .pipe(
        take(1),
        filter((res) => !!res.data),
        map((res: CountUnreadNotificationApiResponse) => res.data?.noOfUnreadNotification),
        tap((count) =>
          this.unreadNotificationsCountSubject.next(count || 0)
        ),
      ).subscribe();
  }

}
