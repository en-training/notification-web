import { NgModule } from '@angular/core';

import { NotificationsLandingComponent } from './components/notifications/notifications-landing/notifications-landing.component';
import { AddNotificationComponent } from './components/notifications/add-notification/add-notification.component';
import { RouterModule, Routes } from '@angular/router';
import { ViewNotificationComponent } from './components/notifications/view-notification/view-notification.component';
import {ListNotificationComponent} from './components/notifications/list-notification/list-notification.component';

const routes: Routes = [

  {
    path: '',
    component: NotificationsLandingComponent,
    children: [
      {
        path: '',
        component: ListNotificationComponent
      },
      {
        path: 'add',
        component: AddNotificationComponent
      },
      {
        path: 'view/:notificationId',
        component: ViewNotificationComponent
      }
    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
