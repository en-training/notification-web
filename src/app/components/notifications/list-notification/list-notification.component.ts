import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationResponse, NotificationStatus } from 'src/app/models/notification.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-list-notification',
  templateUrl: './list-notification.component.html',
  styleUrls: ['./list-notification.component.scss']
})
export class ListNotificationComponent implements OnInit {

  readonly unreadNotificationStatus = NotificationStatus.UN_READ;
  readonly readNotificationStatus = NotificationStatus.READ;

  @Input() notifications: Array<NotificationResponse> = [];

  @Output() notificationEmitter = new EventEmitter<NotificationResponse>;

  constructor(private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit(): void {
    this.notificationService.fetchNotifications()
    .subscribe((res) => {
      this.notifications = res.data?.notificationResponses || [];
    });
  }

  onView(notification: NotificationResponse) {
   // this.notificationEmitter.emit(notification);

   // this.notificationService.countUnreadNotifications();
    this.router.navigateByUrl('view/'+notification.notificationId);
  }

}
