import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationsLandingComponent } from './notifications-landing.component';

describe('NotificationsLandingComponent', () => {
  let component: NotificationsLandingComponent;
  let fixture: ComponentFixture<NotificationsLandingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotificationsLandingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NotificationsLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
