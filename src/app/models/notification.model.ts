
export interface CreateNotificationCommand {
    title: string;
    message: string;
}

export interface CreateNotificationResponse {
    notificationId: string;
    title: string;
    message: string;
}

export type CreateNotificationApiResponse = ApiResponse<CreateNotificationResponse>;

export interface SearchNotificationResponse {
     notificationResponses: Array<NotificationResponse>;
}

export type SearchNotificationApiResponse = ApiResponse<SearchNotificationResponse>;

export interface CountUnreadNotificationResponse {
  noOfUnreadNotification: number;
}

export type CountUnreadNotificationApiResponse = ApiResponse<CountUnreadNotificationResponse>;

export interface MarkNotificationCommand {
  notificationId: string;
  markTo: NotificationStatus;
}

export interface  MarkNotificationResponse {
  notificationId: string;
  status: NotificationStatus;
  message: string;
}
export type MarkNotificationApiResponse = ApiResponse<MarkNotificationResponse>;

export interface NotificationResponse {
    notificationId: string;
    title: string;
    message: string;
    status: NotificationStatus;
}

export enum NotificationStatus {
    READ = 'READ',
    UN_READ = 'UN_READ'
}


export interface Notification {
    title: string;
    message: string;
  }


export interface ApiResponse<T = undefined> {
    data?: T;
    error?: GlobalError;
    validationErrors?: Array<ValidationError>;
    correlationId?: string;
  }

  export interface GlobalError {
    code?: string;
    message?: string;
  }

  export interface ValidationError {
    code: string;
    field: string;
  }

