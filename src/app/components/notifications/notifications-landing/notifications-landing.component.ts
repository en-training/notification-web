import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, map, take } from 'rxjs';
import { NotificationResponse } from 'src/app/models/notification.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-notifications-landing',
  templateUrl: './notifications-landing.component.html',
  styleUrls: ['./notifications-landing.component.scss']
})
export class NotificationsLandingComponent implements OnInit {

  notifications$?: Observable<Array<NotificationResponse>>;

  btnLabel = '';

  unReadNotificationCount$: Observable<number>;

  constructor(private notificationService: NotificationService,
    private translate: TranslateService,
    private router: Router) {
    this.unReadNotificationCount$ = this.notificationService.unreadNotificationsCountSubject.asObservable();
  }

  ngOnInit(): void {
    this.btnLabel = this.translate.instant('PAGES.LANDING.NEW_NOTIFICATION');

    this.notificationService.countUnreadNotifications();

    this.notifications$ = this.notificationService.fetchNotifications()
    .pipe(
      take(1),
      map((res) => {
        return res.data?.notificationResponses || [];
      })
    );

  }

  onNewNotification() {
    this.router.navigateByUrl('add');
  }

  onEmit(notification: NotificationResponse) {
     this.router.navigateByUrl('view/'+notification.notificationId);
  }

}
