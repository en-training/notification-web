import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs';
import { NotificationService } from 'src/app/services/notification.service';
import {CreateNotificationCommand} from '../../../models/notification.model';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {InfoModalComponent} from '../../info-modal/info-modal.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-add-notification',
  templateUrl: './add-notification.component.html',
  styleUrls: ['./add-notification.component.scss']
})
export class AddNotificationComponent implements OnInit {

  errorMessage = '';
  notificationForm: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required]),
    message: new FormControl()
  })
  model: CreateNotificationCommand = {} as CreateNotificationCommand;
  fields: FormlyFieldConfig[] = [
    {
      key: 'title',
      type: 'input',
      props: {
        label: 'Title',
        required: true,
      },
    },
    {
      key: 'message',
      type: 'input',
      props: {
        label: 'Message',
        required: true,
      },
    },
  ];

  constructor(private router: Router,
    private notificationService: NotificationService,
    private translate: TranslateService,
    private modalService: NgbModal) { }

  ngOnInit(): void {

  }

  onSubmit() {
    this.errorMessage = '';
    if(this.notificationForm.valid) {
      //Save notification
      const command = this.notificationForm.value;


      this.notificationService.createNotification(command)
      .pipe(
        take(1)
      )
      .subscribe((res) => {
        this.showInfoModal();
      });
    }
  }

  onBack() {
    this.router.navigateByUrl('');
  }

  private showInfoModal() {
    const modalRef: NgbModalRef = this.modalService.open(InfoModalComponent);
    modalRef.componentInstance.title = 'Information';
    modalRef.componentInstance.message = "Notification Successfully Saved";

    modalRef.result.then((res) => {
      if(res) {
        this.notificationService.countUnreadNotifications();
      }
    })
  }

}
