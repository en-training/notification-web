import { Component } from "@angular/core";
import { NotificationService } from "src/app/services/notification.service";

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent {

    titleCss = 'title';
    ismainTitle = false;

    notificationTitle: string = 'notification title';
    notificationMessage = 'Enter Notification Message';

    firstNumber = 90;
    secondNumber = 12;

    constructor(private notificationService: NotificationService) {

    }


    onSubmit() {
        alert(JSON.stringify(this.notificationService.getNotifications()));
    }
}