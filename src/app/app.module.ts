import { NgModule } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { NotificationsLandingComponent } from './components/notifications/notifications-landing/notifications-landing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationService } from './services/notification.service';
import { AddNotificationComponent } from './components/notifications/add-notification/add-notification.component';
import { AppRoutingModule } from './app-routing.module';
import { ListNotificationComponent } from './components/notifications/list-notification/list-notification.component';
import { ViewNotificationComponent } from './components/notifications/view-notification/view-notification.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { InfoModalComponent } from './components/info-modal/info-modal.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { UpperCasePipe } from './pipes/upper-case.pipe';


export function httpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NotificationsComponent,
    NotificationsLandingComponent,
    AddNotificationComponent,
    ListNotificationComponent,
    ViewNotificationComponent,
    InfoModalComponent,
    UpperCasePipe,
  ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        AppRoutingModule,
        NgbModule,
        FormlyModule.forRoot(),
        FormlyBootstrapModule,
        TranslateModule.forRoot({
          defaultLanguage: 'am',
          loader: {
            provide: TranslateLoader,
            useFactory: httpLoaderFactory,
            deps: [HttpClient],
          },
        }),

    ],
  providers: [
    NotificationService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
