import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs';
import {MarkNotificationCommand, NotificationResponse, NotificationStatus} from 'src/app/models/notification.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-view-notification',
  templateUrl: './view-notification.component.html',
  styleUrls: ['./view-notification.component.scss']
})
export class ViewNotificationComponent implements OnInit {

  notification?: NotificationResponse;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.activatedRoute.params
    .pipe(
      take(1),
    )
    .subscribe(params => {
      const notificationId = params['notificationId'];
      this.notification = this.notificationService.getNotificationById(notificationId);
      const command =  {
        notificationId: notificationId,
        markTo: NotificationStatus.READ
      } as MarkNotificationCommand;
      this.notificationService.markStatusNotification(command)
        .pipe(
          take(1),
        ).subscribe(() => {
          this.notificationService.countUnreadNotifications();
      })
    });
  }

  onBack() {
    this.router.navigateByUrl('');
  }

}
